## TODO List
* Create function that takes 2 YUV sequences and outputs a key and seeded sequence.
  * Input: CIF, QCIF
  * Output: CIF, matrix(size of CIF)
* Create a function that takes a YUV sequence and a key and outputs a YUV sequence.
  * Input: CIF, matrix(size of CIF)
  * Output: QCIF
